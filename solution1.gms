*------------------------------
* definition of deep parameters
*------------------------------

Parameters
gamma    risk aversion
delta    capital depreciation
alphak   marginal capital productivity
beta     time preference
rho      shock autocorrelation
sigma    standard error of the innovation
kbar     determinist capital steady state
cbar     determinist consumption steady state
invbar   determinist investement steady state
;

gamma  = 2  ;
delta  = 0.025 ;
alphak = 0.36 ;
beta   = 0.95 ;
rho    = 0.5 ;
sigma  = 1 ;
kbar   = ((1/beta+delta-1)/alphak)**(1/(alphak-1)) ;
cbar   = kbar**alphak - delta*kbar ;
invbar = kbar*delta ;
display kbar, cbar, invbar ;

*---------------------------
* parameters for projection
*---------------------------
sets nk  number of nodes for capital /1*12 / ;
sets np  number of nodes for price shock / 1*8 / ;

alias (nk,nka,nkb) ;
alias (np,npa,npb) ;

Parameters
k0(nk)      initial capital stock
phi0k(nka,nk)
p0(np)      initial market price level
phi0p(npa,np)
p1(np,npb)  next period  price level
phi1p(npa,np,npb)
pmax
pmin
k0max
k0min
prob(np)

;

k0max = kbar*1.5 ;
k0min = kbar*0.5 ;
k0(nk)  = (k0max+k0min)/2 + (k0max-k0min)/2*  cos(  pi * (1+ (0.5 - ord(nk))/card(nk) ) ) ;

phi0k("1",nk) = 1;
phi0k("2",nk) = 2*k0(nk)/(k0max-k0min)+ (k0max+k0min)/(k0min-k0max);
loop(nka$(ord(nka) GE 3),
phi0k(nka,nk) = 2*phi0k("2",nk)*phi0k(nka-1,nk)-phi0k(nka-2,nk) ;
     );


** Gaussian quadrature derived from Matlab  command [a,b] = qnwnorm(5,0,0.01) with 5 the number of nodes, 0 the expectation and 0.01 the variance
* because this is quite long to solve the 2*5=10 square system of equations sum(i, prob(i)*z(i)**s)=integral(x**s * density(x)) with s = 10 and density the normal law
* in matlab il faut d abord taper cepath='c:\compecon\'; path([cepath 'cetools;' cepath 'cedemos'], path) ;

*p0("1") = -0.4859 ; p0("2") = -0.3582 ; p0("3") = -0.2484 ; p0("4") = -0.1466 ; p0("5") = -0.0485 ;
*p0("10") = 0.4859 ; p0("9") = 0.3582 ; p0("8") = 0.2484 ; p0("7") = 0.1466 ; p0("6") = 0.0485 ;
*p0(np) = 0.5*p0(np) ;
*prob("1") = 0.0 ; prob("2") = 0.0008 ; prob("3") = 0.0191 ; prob("4") = 0.1355 ; prob("5") = 0.3446 ;
*prob("10") = 0.0 ; prob("9") = 0.0008 ; prob("8") = 0.0191 ; prob("7") = 0.1355 ; prob("6") = 0.3446 ;

*p0("1") = -0.4145 ; p0("2") = -0.2802 ; p0("3") = -0.1637 ; p0("4") = -0.0539 ;
*p0("8") = 0.4145 ; p0("7") = 0.2802 ; p0("6") = 0.1637 ; p0("5") = 0.0539 ;

p0("1") = -0.1658 ; p0("2") = -0.1121 ; p0("3") = -0.0655 ; p0("4") = -0.0216 ;
p0("8") = 0.1658 ; p0("7") = 0.1121 ; p0("6") = 0.0655 ; p0("5") = 0.0216 ;
p0(np) = p0(np) ;

prob("1") = 0.0001 ; prob("2") = 0.0096 ; prob("3") = 0.1172 ; prob("4") = 0.3730 ;
prob("8") = 0.0001 ; prob("7") = 0.0096 ; prob("6") = 0.1172 ; prob("5") = 0.3730 ;

pmax = p0("8")*1.5 ;
pmin = -pmax ;

phi0p("1",np) = 1;
phi0p("2",np) = 2*p0(np)/(pmax-pmin)+ (pmax+pmin)/(pmin-pmax);
loop(npa$(ord(npa) GE 3),
phi0p(npa,np) = 2*phi0p("2",np)*phi0p(npa-1,np)-phi0p(npa-2,np) ;
     );
p1(np,npb) = rho*p0(np) + sigma*p0(npb);
phi1p("1",np,npb) = 1 ;
phi1p("2",np,npb) = 2*p1(np,npb)/(pmax-pmin)+ (pmax+pmin)/(pmin-pmax);
loop(npa$(ord(npa) GE 3),
phi1p(npa,np,npb) = 2*phi1p("2",np,npb)*phi1p(npa-1,np,npb)-phi1p(npa-2,np,npb) ;
     );


*------------------
* model definition
*------------------
Variables
lambda(nk,np)
lambda1(nk,np,npb)
c(nk,np)
k1(nk,np)
c1(nk,np,npb)
phi1k(nka,nk,np)
coeff(nk,np)
y(nk,np)
  ;


Equations
eqlambda(nk,np)
eqlambda1(nk,np,npb)
eqc(nk,np)
eqk1(nk,np)
eqcoeff(nk,np)
eqc1(nk,np,npb)
eqphi1k(nka,nk,np)
eqy(nk,np)
  ;


eqk1(nk,np)..            k1(nk,np) =E= (1-delta)*k0(nk) + exp(p0(np))*k0(nk)**(alphak) - c(nk,np) ;

eqphi1k(nka,nk,np)..     phi1k(nka,nk,np) =E=                                                                  1$(ord(nka) eq 1)
                                                     + ( 2*k1(nk,np)/(k0max-k0min)+ (k0max+k0min)/(k0min-k0max))$(ord(nka) eq 2)
                                                    + (2*phi1k("2",nk,np)*phi1k(nka-1,nk,np)-phi1k(nka-2,nk,np))$(ord(nka) GE 3) ;


eqlambda(nk,np)..        lambda(nk,np)  =E= sum((nka,npa), coeff(nka,npa)*phi0k(nka,nk) * phi0p(npa,np) ) ;

eqlambda1(nk,np,npb)..   lambda1(nk,np,npb) =E= sum((nka,npa), coeff(nka,npa)*phi1k(nka,nk,np) * phi1p(npa,np,npb) ) ;

eqc(nk,np)..             c(nk,np)**(-gamma) - lambda(nk,np) =E= 0 ;

eqc1(nk,np,npb)..        c1(nk,np,npb)**(-gamma) - lambda1(nk,np,npb)  =E= 0 ;

eqy(nk,np)..             y(nk,np) =E= k0(nk)**(alphak) ;

eqcoeff(nk,np)..         lambda(nk,np) / beta =E= sum(npb, prob(npb) * (
                                                                        lambda1(nk,np,npb)*( 1 - delta + exp(p1(np,npb))*alphak*k1(nk,np)**(alphak-1) )

                                                                        )
                                                           ) ;

model projection /
eqc.c
eqk1.k1
eqcoeff.coeff
eqc1.c1
eqphi1k.phi1k
eqlambda.lambda
eqlambda1.lambda1
eqy.y
/ ;

coeff.l(nka,npa) = 0.05 ;
coeff.l("1","1") = cbar**(-gamma) ;
c.l(nk,np) = cbar ;
k1.l(nk,np) = kbar ;

phi1k.l("1",nk,np) =  1 ;
phi1k.l("2",nk,np) =  2*k1.l(nk,np)/(k0max-k0min)+ (k0max+k0min)/(k0min-k0max)  ;
loop(nka$(ord(nka) GE 3),
phi1k.l(nka,nk,np) = 2*phi1k.l("2",nk,np)*phi1k.l(nka-1,nk,np)-phi1k.l(nka-2,nk,np) ;
     );
c1.l(nk,np,npb) = cbar ;
lambda.l(nk,np) =  c.l(nk,np)**(-gamma);
lambda1.l(nk,np,npb) = c1.l(nk,np,npb)**(-gamma)  ;
y.l(nk,np) = k0(nk)**(alphak) ;

solve projection using mcp ;

rho    = 0 ;
p1(np,npb) = rho*p0(np) + p0(npb);
phi1p("1",np,npb) = 1 ;
phi1p("2",np,npb) = 2*p1(np,npb)/(pmax-pmin)+ (pmax+pmin)/(pmin-pmax);
loop(npa$(ord(npa) GE 3),
phi1p(npa,np,npb) = 2*phi1p("2",np,npb)*phi1p(npa-1,np,npb)-phi1p(npa-2,np,npb) ;
     );
solve projection using mcp ;


Parameters
coeffs(nka,npa) ;
coeffs(nka,npa) = coeff.l(nka,npa) ;

*----------------
* data simulation
*----------------

Sets t simulated years / 1*51 / ;

Parameters
ps(t)
lambdas(t)
cs(t)
ys(t)
ks(t)
invs(t)

invs(t)
phi0ks(nka,t)
phi0ps(npa,t) ;

ps(t) = 0 ;

loop(t$(ord(t) LT card(t)),

ps(t+1) = rho*ps(t) + normal(0,0.04) ;
) ;

ks(t) = kbar ;


loop(t$(ord(t) LT card(t)),

phi0ks("1",t) = 1;
phi0ks("2",t) = 2*ks(t)/(k0max-k0min)+ (k0max+k0min)/(k0min-k0max);
loop(nka$(ord(nka) GE 3),
phi0ks(nka,t) = 2*phi0ks("2",t)*phi0ks(nka-1,t)-phi0ks(nka-2,t) ;
     );

phi0ps("1",t) = 1;
phi0ps("2",t) = 2*ps(t)/(pmax-pmin)+ (pmax+pmin)/(pmin-pmax);
loop(npa$(ord(npa) GE 3),
phi0ps(npa,t) = 2*phi0ps("2",t)*phi0ps(npa-1,t)-phi0ps(npa-2,t) ;
     );

lambdas(t) = sum((nka,npa), coeffs(nka,npa)*phi0ks(nka,t)*phi0ps(npa,t) ) ;
cs(t)      = lambdas(t)**(-1/gamma) ;
ys(t)      = ks(t)**alphak ;
invs(t)    = exp(ps(t))*ks(t)**(alphak) - cs(t) ;
ks(t+1)    = (1-delta)*ks(t) + exp(ps(t))*ks(t)**(alphak) - cs(t) ;

) ;


display cs, ks, invs, ys ;


*----------------------
* estimation GME
*----------------------

sets k number of support values / 1*3 / ;

Parameters
alphaksv(k)
gammasv(k)
deltasv(k)
betasv(k)
ksv(k)
psv(k)
csv(k) ;

alphaksv("1") = alphak*0.5 ;
alphaksv("2") = alphak ;
alphaksv("3") = alphak*1.5 ;
gammasv("1")  = 0 ;
gammasv("2")  = gamma ;
gammasv("3")  = gamma*2 ;
deltasv("1")  = 0 ;
deltasv("2")  = delta*0.1 ;
deltasv("3")  = delta*1.1 ;
*rhosv("1")    = 0 ;
*rhosv("2")    = 0.5 ;
*rhosv("3")    = 1 ;
*sigmasv("1")  = 0.001 ;
*sigmasv("2")  = 0.04 ;
*sigmasv("3")  = 0.2 ;
betasv("1")   = 0.9 ;
betasv("2")   = beta ;
betasv("3")   = 0.999 ;
ksv("1")      = 0.5*kbar ;
ksv("2")      = kbar ;
ksv("3")      = 1.5*kbar ;
psv("1")      = -0.5 ;
psv("2")      = 0 ;
psv("3")      = 0.5 ;
csv("1")      = 0.5*cbar ;
csv("2")      = cbar ;
csv("3")      = cbar*1.5 ;

Variables
alphake
gammae
deltae
betae

ke(t)
pe(t)

cne(t,np)
entropie
;

Positive variables
pkesv(t,k)
ppesv(t,k)
palphaksv(k)
pgammasv(k)
pdeltasv(k)
pbetasv(k)
 ;

equations
eqalphake
eqgammae
eqdeltae
eqbetae
eqpalphaksv
eqpgammasv
eqpdeltasv
eqpbetasv

eqke(t)
eqpe(t)
eqpkesv(t)
eqppesv(t)

eqcs(t)
eqys(t)
eqbudget(t)

eqentropie
;

eqentropie..     entropie =e=     - sum(k, palphaksv(k)*LOG(1.e-5+palphaksv(k)))
                                  - sum(k, pgammasv(k)*LOG(1.e-5+pgammasv(k)))
                                  - sum(k, pbetasv(k)*LOG(1.e-5+pbetasv(k)))
                                  - sum(k, pdeltasv(k)*LOG(1.e-5+pdeltasv(k)))
                                  - sum((k,t), pkesv(t,k)*LOG(1.e-5+pkesv(t,k)))
                                  - sum((k,t), ppesv(t,k)*LOG(1.e-5+ppesv(t,k)))

 ;


eqcs(t)$(ord(t) LT card(t))..       cs(t)**(-gammae) =E= betae*sum(np, prob(np)*  ( cne(t,np)**(-gammae) * (1-deltae + exp(p0(np))*alphake*ke(t+1)**(alphake-1) )
                                                              )
                                              ) ;
eqys(t)$(ord(t) LT card(t))..       ys(t) =E= exp(pe(t))*ke(t)**alphake ;

eqbudget(t)$(ord(t) LT card(t))..   ke(t+1) =E= (1-deltae)*ke(t) + ys(t) - cs(t) ;

eqke(t)..       ke(t) =E= sum(k, pkesv(t,k)*ksv(k)) ;
eqpkesv(t)..    1     =E= sum(k, pkesv(t,k) ) ;
eqpe(t)..       pe(t) =E= sum(k, ppesv(t,k)*psv(k) ) ;
eqppesv(t)..    1     =E= sum(k, ppesv(t,k)) ;

eqgammae..      gammae =E= sum(k, pgammasv(k)*gammasv(k)) ;
eqpgammasv..    1      =E= sum(k, pgammasv(k)) ;
eqalphake..     alphake =E= sum(k, palphaksv(k)*alphaksv(k)) ;
eqpalphaksv..   1      =E= sum(k, palphaksv(k)) ;
eqbetae..       betae =E= sum(k, pbetasv(k)*betasv(k)) ;
eqpbetasv..     1      =E= sum(k, pbetasv(k)) ;
eqdeltae..      deltae =E= sum(k, pdeltasv(k)*deltasv(k)) ;
eqpdeltasv..    1      =E= sum(k, pdeltasv(k)) ;


model estimation /
eqalphake
eqgammae
eqdeltae
eqbetae
eqpalphaksv
eqpgammasv
eqpdeltasv
eqpbetasv
eqke
eqpe
eqpkesv
eqppesv
eqcs
eqys
eqbudget
eqentropie
/ ;


alphake.l = alphak ;
gammae.l  = gamma ;
deltae.l  = delta*0.1 ;
betae.l   = beta ;
palphaksv.l(k) = 1/3 ;
pgammasv.l(k)  = 1/3 ;
pdeltasv.l(k)  = 1/3 ;
pbetasv.l(k)   = 1/3 ;

ke.l(t)        = kbar ;
pe.l(t)        = 0 ;
pkesv.l(t,k)   = 1/3  ;
ppesv.l(t,k)   = 1/3 ;

cne.l(t,np) = cbar ;
cne.lo(t,np) = 0.1 ;
ke.lo(t)     = kbar*0.5 ;

solve estimation using nlp maximising entropie ;

parameters
titi(t) ;
titi(t)$(ord(t) LT card(t)) =     -cs(t)**(-gammae.l) + betae.l*sum(np, prob(np)*  ( cne.l(t,np)**(-gammae.l) * (1-deltae.l + exp(p0(np))*alphake.l*ke.l(t+1)**(alphake.l-1) )
                                                              )
                                              ) ;
display titi ;
