% SUPPLY_MODEL.M
% This program describes the model 

function focs = supply_model(para, state, vari)
  smp = 30;
  para.beta = 0.95;
  para.w = 1;
  
%   Write equations
  d1 = smp-1;
  for t = 1 : d1
    f1 = exp(state.P(t))*exp(state.Z(t))*state.K(t)^para.alpha*vari.L(t)^para.theta - vari.C(t) - vari.I(t) - para.w * vari.L(t);         % budget contraint 
    f2 = vari.I(t) + (1-para.delta) * state.K(t)  - state.K(t+1);                                                    % capital accumulation
    f3 = vari.C(t)^(-para.gama) - para.beta*vari.C(t+1)^(-para.gama)* ...
         (1 + exp(state.P(t+1))*para.alpha*exp(state.Z(t+1))*state.K(t+1)^(para.alpha-1)*vari.L(t+1)^para.theta ...
          - para.delta);                                                                                             % intertemporal FOC  
    f4 = para.w - exp(state.P(t))*para.theta*exp(state.Z(t))*state.K(t)^para.alpha*vari.L(t)^(para.theta-1);         % labor demand
%     grid_count = 10;
%     EZ(t) = 0;
%     for n = 1:grid_count
%       [grid_value,grid_weight] = qnwnorm(grid_count,0,0.0016);  
%       EZ(t)=EZ(t) + grid_weight(n)*(para.rho*state.Z(t) + grid_value(n));               % technology shock process
%     end
    
%     f5 = state.Z(t+1) - EZ(t);
    f5 = 0;    
    f6 = 0;

  % Create function f
    focs = [f1;f2;f3;f4;f5;f6];
  end

