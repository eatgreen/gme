% This program computes the steady state of the model 

function ss = supply_model_ss(para)

para.beta = 0.95;
w = 1;

ss.Z = 0;
ss.P = 0;
ss.Y = (para.theta^para.theta * (para.alpha /(1/para.beta-1+para.delta))^para.alpha)^(1/(1-para.alpha-para.theta));
ss.K = para.alpha * ss.Y / (1/para.beta-1+para.delta);
ss.L = para.theta * ss.Y / 1;
ss.I = para.delta*ss.K;
ss.C = ss.K^para.alpha-ss.I-w*ss.L;

ss.Z_next = ss.Z;
ss.K_next = ss.K;
ss.C_next = ss.C;
ss.I_next = ss.I;
ss.L_next = ss.L;



