function p_opt  = entropy_opt(prior, vari, m)
  smp = 30;
  option_fmincon = optimoptions('fmincon', 'Algorithm', 'interior-point', ...
                   'Display', ...
                   'iter-detailed', 'TolFun', 1e-10, 'TolX', 1e-10, 'MaxIter', 5000,'MaxFunEvals',30000);
%   'MaxFunctionEvaluations',200000,                              
  prior_v=make_vector(prior,m);

  % Set initial probability
  p_v_init=zeros(size(prior_v));
  for i = 1:size(p_v_init)
    p_v_init(i)=1/m;
  end
  
  lb = zeros(1, 5*m+3*m*smp,1);
  ub = ones(1, 5*m+3*m*smp,1);
  [p_v_opt,fval,exitflag] = fmincon(@(p_v)entropy_objective(p_v, m), p_v_init, [],[],[],[],lb,ub, @(p_v)constraint (p_v, prior_v, vari, m), option_fmincon);
    if exitflag <= 0         
      fprintf('not converge (status %g, f=%g)\n', exitflag, fval);       
    end
  p_opt = make_obj(p_v_opt,m);
end

