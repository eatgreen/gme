function obj_v=make_vector(prior,m)
  smp=30;
  para_number=5;
  state_number=3;
  obj_v_size=para_number*m+state_number*m*smp;
  obj_v=zeros(obj_v_size,1);

  prior_v.K=reshape(prior.K,numel(prior.K),1);
  prior_v.Z=reshape(prior.Z,numel(prior.Z),1);
  prior_v.P=reshape(prior.P,numel(prior.P),1);

  obj_v(1:m)=prior.theta;
  obj_v(m+1:2*m)=prior.alpha;
  obj_v(2*m+1:3*m)=prior.gama;
  obj_v(3*m+1:4*m)=prior.delta;
  obj_v(4*m+1:5*m)=prior.rho;
  obj_v(5*m+1:5*m+m*smp)=prior_v.K;
  obj_v(5*m+m*smp+1:5*m+2*m*smp)=prior_v.Z;
  obj_v(5*m+2*m*smp+1:end)=prior_v.P;
end