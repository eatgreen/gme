function [c, ceq] = constraint (p_v, prior_v, vari, m)
  smp = 30;
  proba_size=5+3*smp;
  proba = zeros(proba_size,1);
  for j=1:proba_size
      for i = 1 : m    % m: the number of proposed priors
        proba(j) = proba(j) + p_v(i+m*(j-1));
      end
  end
  
  p=make_obj(p_v,m);
  prior=make_obj(prior_v,m);
  
  para.theta = 0;
  para.alpha = 0;
  para.gama = 0;
  para.delta = 0;
  para.rho= 0;
  state.K = zeros(smp,1);
  state.Z = zeros(smp,1);
  state.P = zeros(smp,1);
    
  for i = 1 : m
    para.theta = para.theta + p.theta(i) * prior.theta(i);
    para.alpha = para.alpha + p.alpha(i) * prior.alpha(i);
    para.gama = para.gama + p.gama(i) * prior.gama(i);
    para.delta = para.delta + p.delta(i) * prior.delta(i);
    para.rho = para.rho + p.rho(i) * prior.rho(i);
      for j = 1 : smp
        state.Z(j) = state.Z(j) + p.Z(i,j) * prior.Z(i,j);
        state.K(j) = state.K(j) + p.K(i,j) * prior.K(i,j);
        state.P(j) = state.P(j) + p.P(i,j) * prior.P(i,j);          
      end
  end
    
  foc = supply_model(para, state, vari);
  
  c=[];
%   ceq = [proba - 1; foc];
  ceq = [1-proba; -foc];
end