function obj = make_obj(p_v,m)
  smp = 30;
  para_number=5;
  state_number=3;
  obj_v_size=para_number*m+state_number*m*smp;

  obj.theta = p_v(1:m);
  obj.alpha = p_v(m+1:2*m);
  obj.gama  = p_v(2*m+1:3*m);
  obj.delta = p_v(3*m+1:4*m);
  obj.rho   = p_v(4*m+1:5*m);
  obj.K = reshape(p_v(5*m+1:5*m+m*smp),m,[]);
  obj.Z = reshape(p_v(5*m+m*smp+1:5*m+2*m*smp),m,[]);
  obj.P = reshape(p_v(5*m+2*m*smp+1:end),m,[]);
end