function entropy = entropy_objective (p_v, m)
%     entropy = sum(p_v(:).*log(p_v(:)) );
%   entropy = p_v' * log(1.e-10 + p_v);

  nsample=size(p_v,1)/m;
  prob_joint=1;
  for i=1:nsample
      
      var_prob=p_v(1+m*(i-1):m+m*(i-1));
      prob_joint=prob_joint.*var_prob;
  end
    entropy = prob_joint' * log(1.e-10 + prob_joint);

end