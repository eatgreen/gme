% generate data 
% alpha=0.37 theta=0.25 gama=2 delta=0.025 rho=0.85
smp = 30;         % sample size 
drop = 100;       % number of observations to be dropped 
tt = drop + smp;  % total number of observations 

load('vv_high.mat')
data  = [c_obs, i_obs, l_obs];
vv = data';

vari.C = vv(1,drop+1:tt);
vari.I = vv(2,drop+1:tt);
vari.L = vv(3,drop+1:tt);

% theta=0.25 alpha=0.37 gama=2 delta=0.025 rho=0.85

% Set parameter priors
m=10;
% prior.theta = [0.25, 0.25, 0.25];
% prior.alpha = [0.37, 0.37, 0.37];
% prior.gama = [2, 2, 2];
% prior.delta = [0.025, 0.025, 0.025];
% prior.rho = [0.85, 0.85, 0.85];

prior.theta = [0.1, 0.3, 0.5, 0.1, 0.3, 0.5, 0.1, 0.3, 0.5, 0];
prior.alpha = [0.1, 0.3, 0.5, 0.1, 0.3, 0.5, 0.1, 0.3, 0.5, 0];
prior.gama = [2, 2, 2, 2, 2, 2, 2, 2, 2, 2];
prior.delta = [0.025, 0.025, 0.025, 0.025, 0.025, 0.025, 0.025, 0.025, 0.025, 0.025];
prior.rho = [0.85, 0.85, 0.85, 0.85, 0.85, 0.85, 0.85, 0.85, 0.85, 0.85];


init.theta=0;
init.alpha=0;
init.gama=0;
init.delta=0;
init.rho=0;
  
for i = 1:m
  init.theta=init.theta+1/m*prior.theta(i);
  init.alpha=init.alpha+1/m*prior.alpha(i);
  init.gama=init.gama+1/m*prior.gama(i);
  init.delta=init.delta+1/m*prior.delta(i);
  init.rho=init.rho+1/m*prior.rho(i);
end

ss = supply_model_ss(init);

prior.K=zeros(m,smp);
prior.Z=zeros(m,smp);
prior.P=zeros(m,smp);
% Set state priors
for j = 1: smp
  prior.K(:,j) = ss.K*[0.47, 1, 1.72, 0.47, 1, 1.72, 0.47, 1, 1.72, 1];            
  prior.Z(:,j) = ss.Z*[-0.14, 1, 0.14, -0.14, 1, 0.14, -0.14, 1, 0.14, 1];
  prior.P(:,j) = ss.P*[-0.05, 1, 0.05, -0.05, 1, 0.05, -0.05, 1, 0.05, 1];
end  

% Maximize entropy
p = entropy_opt(prior, vari, m);

% Compute the final results  
para.theta = 0;
para.alpha = 0;
para.gama = 0;
para.delta = 0;
para.rho= 0;
state.K = zeros(smp,1);
state.Z = zeros(smp,1);
state.P = zeros(smp,1);
    
for i = 1 : m
  para.theta = para.theta + p.theta(i) * prior.theta(i);
  para.alpha = para.alpha + p.alpha(i) * prior.alpha(i);
  para.gama = para.gama + p.gama(i) * prior.gama(i);
  para.delta = para.delta + p.delta(i) * prior.delta(i);
  para.rho = para.rho + p.rho(i) * prior.rho(i);
    for j = 1 : smp
      state.Z(j) = state.Z(j) + p.Z(i,j) * prior.Z(i,j);
      state.K(j) = state.K(j) + p.K(i,j) * prior.K(i,j); 
      state.P(j) = state.P(j) + p.P(i,j) * prior.P(i,j);
    end
end

display(p);
display(para);



